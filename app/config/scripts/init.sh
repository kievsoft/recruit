#!/bin/bash

composer install --verbose --no-progress 2>&1

# this will occur error in fresh install
$(which php)  artisan storage:link
# end
$(which php)  artisan config:cache
$(which php)  artisan route:cache
